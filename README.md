# README #

A simple docker-compose file to run [FileRun](https://www.filerun.com/) on your own server in a Nginx reverse proxy architecture 
with the SSL certificates configured automatically through LetsEncrypt.

FileRun is a self-hosted file-sync and sharing app which can be installed on any private Linux, Mac, Windows server or Web Hosting account

The reverse proxy architecture with auto SSL is enabled through the excellent [docker-letsencrypt-nginx-proxy-companion](https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion) repository

The original FielRun Docker compose file can be found at this [repo](https://github.com/filerun/docker).

### How to Run? ###

Before running the container, make sure to create two empty folders:

`mkdir /filerun /filerun/html /filerun/user-files`

*Parameters:*

Replace all the usual usernames and passwords in the mariadb and filerun image.

To ensure SSL certificate is installed and configured correctly, don't forget to set the following parameters in the 'afian/filerun' image

- *VIRTUAL_HOST: <yourdomain.tld>*
- *LETSENCRYPT_HOST: <yourdomain.tld>*

In the jrcs/letsencrypt-nginx-proxy-companion image, set your email id in the environment paramaters.

- *DEFAULT_EMAIL: <youremailid>*

To run this setup, run the command : `docker-compose up -d`

The default username/password on the first-run is superuser/superuser. 
You will be required to change this on the first login.

### 413 Request Entity Too Large ###

On the first run, the uploads to your FileRun instance most likely will fail with the error **413 Request Entity Too Large** 
This is because the request made is too large to be processed by the Nginx web server. 

Follow the steps below to resolve this

`echo "client_max_body_size 10G;" > uploadsize.conf`

`docker cp uploadsize.conf <nginx_container>:/etc/nginx/conf.d/uploadsize.conf`

`docker restart <nginx_container>`